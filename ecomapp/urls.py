from django.urls import path, include
from ecomapp.views import indexPage

urlpatterns = [
    path('', indexPage, name='base')
]
